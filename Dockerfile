# Most ideas come from https://stackoverflow.com/questions/54437030/how-can-i-create-a-docker-image-to-run-both-python-and-r

# See if we can do better
FROM ubuntu:latest 

#block prompts
ENV DEBIAN_FRONTEND=noninteractive

# Install R and Python and essential dependencies
RUN apt-get update && apt-get install -y --no-install-recommends build-essential r-base python3.6 python3-pip python3-setuptools python3-dev


ADD /lib/requirements.txt requirements.txt

RUN pip3 install --upgrade pip \
    && pip install --upgrade -r /requirements.txt \
    && rm /requirements.txt \
    && rm -rf ~/.cache/pip \
    && rm -rf /src

# Install R libraries
RUN Rscript -e 'chooseCRANmirror(graphics=FALSE, ind=1); install.packages("dplyr", dependencies=TRUE);'

# Install bash dependencies
RUN  apt-get update \
  && apt-get install -y wget \
  && rm -rf /var/lib/apt/lists/*

# Install java
RUN apt-get update && \
    apt-get install -y openjdk-8-jdk && \
    apt-get install -y ant && \
    apt-get clean;

ENV JAVA_HOME /usr/lib/jvm/java-8-openjdk-amd64/
RUN export JAVA_HOME


COPY ./ ./