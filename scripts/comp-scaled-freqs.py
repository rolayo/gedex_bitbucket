import pandas as pd
import numpy as np
from sklearn.preprocessing import MinMaxScaler as minmax
from optparse import OptionParser
import os
from joblib import Parallel, delayed
import multiprocessing


#Parameters 

parser = OptionParser()
parser.add_option("-i", dest="inF",help='File with interactions in identifier format', metavar="PATH")
parser.add_option("-o", dest='outF', help='File to save scaled frequencies', metavar="PATH")
parser.add_option("-n", dest="ncore", help='Number of cores')

(options, args) = parser.parse_args()

if len(args) > 0:
    parser.error("Please indicate all necessary inputs")
    sys.exit(1)

num_cores = int(options.ncore)
inFile = options.inF
outFile = options.outF
features = ['gcf','dcf','icf']

#Functions 

def calculatefreqs(pmid,interaction):
    gene,dis = interaction.split("\|")
    gcf = corFreqs[0][gene]
    dcf = corFreqs[1][dis]
    icf = corFreqs[2][interaction]
    interlist = [pmid,gene,dis,gcf,dcf,icf]
    return interlist

def scale(data,features):
    scdata = pd.DataFrame(index=data.index,columns=features)
    dat = data[features]
    scaler = minmax()
    scdata = scaler.fit_transform(dat)
    return(scdata)

#Reading files 

idMat = pd.read_csv(inFile,delimiter='\t')
idMat['interaction'] = idMat['gene_id'] + "\|" + idMat['disease_id']

#Compute frequencies 

##Summary 

gcf = idMat.gene_id.value_counts()
dcf = idMat.disease_id.value_counts()
icf = idMat.interaction.value_counts()
corFreqs = [gcf,dcf,icf]

##Parallel processing

inputs = [(pm,itr) for pm in idMat['pmid'] for itr in idMat.loc[idMat['pmid'] == pm,'interaction']]
results = Parallel(n_jobs=num_cores)(delayed(calculatefreqs)(pars[0],pars[1]) for pars in inputs)
resDat = pd.DataFrame(results)
resDat.columns = ['pmid','gene_id','disease_id','gcf','dcf','icf']

##Compute interaction probability

resDat['cps'] = resDat['icf']/((resDat['gcf']+resDat['dcf'])/2) # Within corpus probability score

#Scaling (mmx)

resDat[features] = scale(resDat,features)

#Save output

resDat.to_csv(outFile,sep='\t',index=False)
