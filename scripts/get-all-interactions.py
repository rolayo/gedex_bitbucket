import re
from optparse import OptionParser
from collections import defaultdict
from numpy import unique as u

# Receive parametrs
parser = OptionParser()
parser.add_option("-i", dest="inF",help="Input file", metavar="PATH")
parser.add_option("-o", dest="ouF",help="Output file", metavar="PATH")

(options, args) = parser.parse_args()

if len(args) > 0:
    parser.error("Please indicate an input directory")
    sys.exit(1)

# Create an object containing all the sentences of each abstract
sentences = defaultdict(list) # key: pmid - value: [snum,sentence]
with open(options.inF,'r') as f:
    for line in f:
        l=line.split('\t')
        ids=re.search('(.*)\.(\d+)',str(l[0]))
        pmid,snum=ids.group(1),ids.group(2)
        sentences[pmid].append([snum,l[1]])

# Generate all possible combinations between genes and diseases by sentence
out=open(options.ouF,"w")
out.write("\t".join(["pmid","gene","disease","snum","sentence\n"]))
for key in sentences.keys():
    for sen in sentences[key]:
        genes=re.findall(r'<g>(.*?)</g>',sen[1])
        dis=re.findall(r'<d>(.*?)</d>',sen[1])
        if(len(genes)>0 and len(dis)>0):
            for g in u(genes):
                for d in u(dis):
                    if(len(g)>0 and len(d)>0):
                        newline = "\t".join([key,g,d,sen[0],sen[1]+"\n"])
                        out.write(newline)
out.close()


