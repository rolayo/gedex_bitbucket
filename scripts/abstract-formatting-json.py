import os
import re
import glob
import json
import pandas as pd
from optparse import OptionParser
from joblib import Parallel, delayed


parser = OptionParser()

parser.add_option("-i", dest="inF",help="input directory", metavar="PATH")
parser.add_option('-c', dest='ncore', help="number of cores", type=int)

(options, args) = parser.parse_args()

if len(args) > 0:
    parser.error("Please indicate an input directory")
    sys.exit(1)

mainDir=options.inF
num_cores = options.ncore

######################################### Formatting function ######################################

def check_download(file_down):
	with open(file_down, encoding='utf-8', errors='replace') as att:
		att_lines = att.readlines()
		if len(att_lines)==0 or att_lines[0].startswith('<?xml'):
			return(1)
		else:
			return(0)

def remove_overlap(annotations):
	k=0
	#outAn=[]
	for i in range(1,len(annotations)-1,1):
		#Offsets 
		an1 = annotations[i-1-k].split('\t')
		an2 = annotations[i-k].split('\t')
		s1,e1 = int(an1[1]),int(an1[2])
		s2,e2 = int(an2[1]),int(an2[2])
		if (s2>s1 and e2<e1) or s2<e1:
			annotations.pop(i-k)
			k+=1
	return(annotations)

def format_abstract(file):
	outF=re.sub("\.abs",".fmt",file)
#	print(file)
	
	#Validate download
	not_json = check_download(file)
	if not_json:
		pm_f = file.split('/')[-1]
		pm = re.search('(\d+?)\.abs', pm_f).group(1) 
		return('nf-{}'.format(pm)) 
	
	with open(file, encoding='utf-8', errors='replace') as pubtator_download:
		absjson = json.load(pubtator_download)
	
	if len(absjson) == 0:
		pm_f = file.split('/')[-1]
		pm = re.search('(\d+?)\.abs', pm_f).group(1)
		return('na-{}'.format(pm))

	absdata = absjson[0]
	abs_text = absdata['text']
	annotations = absdata['denotations']
	
	if len(annotations) == 0:
		return('na-{}'.format(absdata['sourceid']))

	annots_list = []
	for dictionary in annotations:
		obj_info = dictionary['obj'].split(':')
		
		if len(obj_info) == 3:
			obj = obj_info[0]
			id_ = obj_info[2]
		else:
			obj = obj_info[0]
			id_ = obj_info[1]

		start = dictionary['span']['begin']
		end = dictionary['span']['end']

		annot_line = [absdata['sourceid'], start, end, abs_text[start:end], obj, id_]
		annots_list.append(annot_line)

	annot_df = pd.DataFrame(annots_list)
	annot_df.sort_values(by=[1,2], inplace=True)
	sorted_annots = ['\t'.join(str(element) for element in line) for line in annot_df.values]
	no_overlap_annots = remove_overlap(sorted_annots)

	abs_line = '|'.join([absdata['sourceid'], 't', abs_text]) + '\n'
	all_annot_line = '\n'.join(no_overlap_annots) + '\n'

	newline = ''.join([abs_line, all_annot_line])
	
	with open(outF, 'w', encoding='utf-8', errors='replace') as newfile:
		newfile.write(newline)

	return('formatted-{}'.format(absdata['sourceid']))
######################################### Parallel formatting ####################################

abs_files = glob.glob(os.path.join(mainDir, '*.abs'))

#print(abs_files)
results = Parallel(n_jobs=num_cores)(delayed(format_abstract)(p) for p in abs_files)

nformat = [nfabs.split('-')[1] for nfabs in results if nfabs.startswith('nf')]
nannot = [nfabs.split('-')[1] for nfabs in results if nfabs.startswith('na')]
results = [fabs for fabs in results if fabs.startswith('formatted')]

with open('pubtator.log', 'w') as newfile:
	newfile.write('\n'.join(nformat))

if len(abs_files) == len(results):
	print('Finished formatting abstracts...')
else:
	print('{} were able to be formatted ({} were not. {} were downloaded incorrectly)'.format(len(results), len(nformat+nannot), len(nformat)))
