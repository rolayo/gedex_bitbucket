#!/bin/bash

# Downloads

make download-pubtator coreNum=$1

n=$(wc -l pubtator-downloads.sh | grep -o -e '[0-9]*')
dwld=$(find "../input-corpora/all-abstract-files" -maxdepth 1 -name '*abs' | wc -l)

while [ $dwld -lt $n ]
do
	dwld=$(find "../input-corpora/all-abstract-files" -maxdepth 1 -name '*abs' | wc -l)
	i=$((n - dwld))
	echo "Downloaded abstracts: $dwld"
	echo "Missing abstracts: $i"
	sleep 15
done

echo " ==> Pubtator download finished."

make format-pubtator
rm pubtator-downloads.sh
rm *ptdw*

if [ -s "pubtator.log" ]
then
	make complete-downloads

	prev=$(find "../input-corpora/all-abstract-files" -maxdepth 1 -name '*abs' | wc -l)
	echo "Complete abstracts previously downloaded: $prev"
	dwld=$(find ./incomplete-abstracts -maxdepth 1 -name '*abs' | wc -l)
	n=$(wc -l pubtator-downloads.sh | grep -o -e '[0-9]*')
	echo "Missing abstracts: $n"
	s=0
	stop=5
	while [ $dwld -lt $n ]
	do
		if [ "$j" == "$i" ]
		then
			((s++))
		fi

		if [ "$s" == "$stop" ]
		then
			echo "Sorry, $i abstracts from pubtator.log could not be downloaded from PubTator API"
			echo "GeDEx will continue with downloaded abstracts"
			break
		fi

		i=$((n - dwld))
		dwld=$(find ./incomplete-abstracts -maxdepth 1 -name '*abs' | wc -l)
		echo "Downloaded abstracts: $dwld"
		echo "Missing abstracts: $i"
		sleep 10
		j=$i
	done

	echo "Formatting missing abstracts .."
	python abstract-formatting-json-v2.py -i incomplete-abstracts -c "$1"
	mv incomplete-abstracts/* "../input-corpora"/all-abstract-files
	echo " ==> Pubtator download and formatting finished ."
else
	echo " ==> Pubtator downlo ad and formatting finished."
fi

make retrieve-cleaned-raw-abstracts

# Format annotations

make change-annotation-format

# Split abstracts in sentences 

make split-sentences

# Retrieve interactions

echo "Note: only sentences with simultanous mentions of genes and diseases will be conserved"
make retrieve-interactions

# Convert to ids

make convert-to-ids

# Compute freqs

make compute-scaled-freqs

# Classify interactions

make classify

# Generate final output with original IDs

make revert-ids

# Clean temporary files

make clean-dir
